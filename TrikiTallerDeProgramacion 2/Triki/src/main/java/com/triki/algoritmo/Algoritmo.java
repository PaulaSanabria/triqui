package com.triki.algoritmo;

import org.apache.log4j.Logger;

public class Algoritmo {

    static final Logger logger = Logger.getLogger(Algoritmo.class);
    private final static int TAM = 3;
    private final int tablero[][] = new int[TAM][TAM];
    private int estadoGanador = -1;

    public Algoritmo() {

    }

    public int[][] devolverTablero() {
        return tablero;
    }

    public void empezarPartida() {
        for (int n = 0; n < TAM; n++) {
            for (int m = 0; m < TAM; m++) {
                tablero[n][m] = -1;
            }
        }
        estadoGanador = -1;
    }

    public void verificarPosicion(int n, int m) {
        if (n >= 0 && m >= 0 && n < TAM && m < TAM && tablero[n][m] == -1) {
            if (estadoGanador == -1) {
                tablero[n][m] = 0;
                estadoGanador = validarGanadorPartida();
                ponerFichaOrdenador();
            }
        }
    }

    //metodo validar Ganador
  

    public int obtenerGanador() {
        return estadoGanador;
    }

    public int validarGanadorPartida() {
 if (tablero[0][0] != -1 && tablero[0][0] == tablero[1][1]
 && tablero[0][0] == tablero[2][2]) {
 return tablero[0][0];
 }
 if (tablero[0][2] != -1 && tablero[0][2] == tablero[1][1]
 && tablero[0][2] == tablero[2][0]) {
 return tablero[0][2];
 }
 for (int n = 0; n < TAM; n++) {
 if (tablero[n][0] != -1 && tablero[n][0] == tablero[n][1]
 && tablero[n][0] == tablero[n][2]) {
 return tablero[n][0];
 }
 if (tablero[0][n] != -1 && tablero[0][n] == tablero[1][n]
 && tablero[0][n] == tablero[2][n]) {
 return tablero[0][n];
 }
 }
 return -1;
 }

private boolean definirTableroCompleto() {
 for (int n = 0; n < TAM; n++) {
 for (int m = 0; m < TAM; m++) {
 if (tablero[n][m] == -1) {
 return false;
 }
 }
 }
 return true;
 }
private boolean finalizarPartida() {
 return definirTableroCompleto() || validarGanadorPartida() != -1;
 }
private void ponerFichaOrdenador() {
 if (!finalizarPartida()) {
 int f = 0, c = 0;
 int v = Integer.MIN_VALUE;
 int aux;
 for (int n = 0; n < TAM; n++) {
 for (int m = 0; m < TAM; m++) {
 if (tablero[n][m] == -1) {
 tablero[n][m] = 1;
 aux = obtenerMinimo();
 if (aux > v) {
 v = aux;
 f = n;
 c = m;
 }
 tablero[n][m] = -1;
 }
 }
 }
 tablero[f][c] = 1;
 }
 estadoGanador = validarGanadorPartida();
 }


    

    private int obtenerMaximo() {
        if (finalizarPartida()) {
            if (validarGanadorPartida() != -1) {
                return -1;
            } else {
                return 0;
            }
        }
        int v = Integer.MIN_VALUE;
        int aux;
        for (int n = 0; n < TAM; n++) {
            for (int m = 0; m < TAM; m++) {
                if (tablero[n][m] == -1) {
                    tablero[n][m] = 1;
                    aux = obtenerMinimo();
                    if (aux > v) {
                        v = aux;
                    }
                    tablero[n][m] = -1;
                }
            }
        }
        return v;
    }

    private int obtenerMinimo() {
        if (finalizarPartida()) {
            if (validarGanadorPartida() != -1) {
                return 1;
            } else {
                return 0;
            }
        }
        int v = Integer.MAX_VALUE;
        int aux;
        for (int n = 0; n < TAM; n++) {
            for (int m = 0; m < TAM; m++) {
                if (tablero[n][m] == -1) {
                    tablero[n][m] = 0;
                    aux = obtenerMaximo();
                    if (aux < v) {
                        v = aux;
                    }
                    tablero[n][m] = -1;
                }
            }
        }
        return v;
    }

}
